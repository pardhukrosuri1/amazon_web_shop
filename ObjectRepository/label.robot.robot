*** Settings ***
Resource          ../Global/super.robot

*** Variables ***
${label.amazon.added_to_cart_success_message}    //*[normalize-space(text())="Added to Cart"]
${label.amazon.single_product_price}    //div[@class="a-row a-spacing-micro"]//span[@class="a-color-price hlb-price a-inline-block a-text-bold"]
${label.amazon.multiple_product_price}    //div[@class="a-row a-spacing-micro"]//span[@class="a-color-price hlb-price a-inline-block a-text-bold"]
${label.amazon.empty_cart}    //h2[contains(.,"Your Amazon Basket is empty")]
