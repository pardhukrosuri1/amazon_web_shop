*** Settings ***
Resource          ../Global/super.robot

*** Variables ***
${button.amazon.add_to_cart}    //input[@name="submit.add-to-cart"]
${button.amazon.search}    //input[@value="Go"]
${button.amazon.cart}    //div[@class="a-row huc-v2-table-col"]//a[@class="a-button-text" and @role="button" and contains(.,"Cart")]
${button.amazon.delete_items}    //input[@value="Delete"]
${button.lighting_deal.add_to_cart}    //a[@title="Add to Shopping Cart"]
