from robot.libraries.BuiltIn import BuiltIn
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
import xlrd
import calendar
import time
import pyperclip
import json
import os
from pathlib import Path
import xlwt
import xlrd
from xlutils.copy import copy
                
class CustomLibrary(object):

        def __init__(self):
            pass

        @property
        def _sel_lib(self):
            return BuiltIn().get_library_instance('SeleniumLibrary')

        @property
        def _driver(self):
            return self._sel_lib.driver

        def get_abspath(self):
            localdir = os.path.dirname(os.path.abspath(__file__))
            path = Path(localdir)
            file_path = path.parent
            default_directory = os.path.join(file_path, 'FileDownload')
            return str(default_directory)
        
        def open_chrome_browser(self,url):
            """Return the True if Chrome browser opened """
            default_directory =  self.get_abspath()
            selenium = BuiltIn().get_library_instance('SeleniumLibrary')
            try:
                options = webdriver.ChromeOptions()
                caps = DesiredCapabilities.CHROME
                caps["pageLoadStrategy"] = "eager"
                options.add_argument("disable-extensions")
                options.add_experimental_option('prefs', {
                    'credentials_enable_service': False,
                    'download.default_directory' : default_directory,
                    'profile': {
                        'password_manager_enabled': False
                    }
                })
                options.add_experimental_option("excludeSwitches",["enable-automation","load-extension"])
                selenium.create_webdriver('Chrome',chrome_options=options, desired_capabilities=caps )
                selenium.go_to(url)
                return True
            except:
                return False
            
        def get_ms_excel_row_values_into_dictionary_based_on_key(self,filepath,keyName,sheetName=None):
            """Returns the dictionary of values given row in the MS Excel file """
            workbook = xlrd.open_workbook(filepath)
            snames=workbook.sheet_names()
            dictVar={}
            if sheetName==None:
                sheetName=snames[0]      
            if self.validate_the_sheet_in_ms_excel_file(filepath,sheetName)==False:
                return dictVar
            worksheet=workbook.sheet_by_name(sheetName)
            noofrows=worksheet.nrows
            dictVar={}
            headersList=worksheet.row_values(int(0))
            for rowNo in range(1,int(noofrows)):
                rowValues=worksheet.row_values(int(rowNo))
                if str(rowValues[0])!=str(keyName):
                    continue
                for rowIndex in range(0,len(rowValues)):
                    cell_data=rowValues[rowIndex]
                    """if(str(cell_data)=="" or str(cell_data)==None):
                        continue"""                    
                    cell_data=self.get_unique_test_data(cell_data)
                
                    dictVar[str(headersList[rowIndex])]=str(cell_data)
            return dictVar

        def get_all_ms_excel_row_values_into_dictionary(self,filepath,sheetName=None):
            """Returns the dictionary of values all row in the MS Excel file """
            workbook = xlrd.open_workbook(filepath)
            snames=workbook.sheet_names()
            all_row_dict={}
            if sheetName==None:
                sheetName=snames[0]      
            if self.validate_the_sheet_in_ms_excel_file(filepath,sheetName)==False:
                return all_row_dict
            worksheet=workbook.sheet_by_name(sheetName)
            noofrows=worksheet.nrows
            headersList=worksheet.row_values(int(0))
            for rowNo in range(1,int(noofrows)):
                each_row_dict={}
                rowValues=worksheet.row_values(int(rowNo))
                for rowIndex in range(0,len(rowValues)):
                    cell_data=rowValues[rowIndex]
                    if(str(cell_data)=="" or str(cell_data)==None):
                        continue
                    cell_data=self.get_unique_test_data(cell_data)
                    each_row_dict[str(headersList[rowIndex])]=str(cell_data)
                all_row_dict[str(rowValues[0])]=each_row_dict
            return all_row_dict

        def get_unique_test_data(self,testdata):
            """Returns the unique if data contains unique word """
            ts = calendar.timegm(time.gmtime())
            unique_string=str(ts)
            testdata=testdata.replace("UNIQUE",unique_string)
            testdata=testdata.replace("Unique",unique_string)
            testdata=testdata.replace("unique",unique_string)
            return testdata

        def validate_the_sheet_in_ms_excel_file(self,filepath,sheetName):
            """Returns the True if the specified work sheets exist in the specifed MS Excel file else False"""
            workbook = xlrd.open_workbook(filepath)
            snames=workbook.sheet_names()
            sStatus=False        
            if sheetName==None:
                return True
            else:
                for sname in snames:
                    if sname.lower()==sheetName.lower():
                        wsname=sname
                        sStatus=True
                        break
                if sStatus==False:
                    print ("Error: The specified sheet: "+str(sheetName)+" doesn't exist in the specified file: " +str(filepath))
            return sStatus

        def wait_until_element_clickable(self,locator):
            """ An Expectation for clicking that an element with xpath or id as a locator"""
            if locator.startswith("//") or locator.startswith("(//"):
               WebDriverWait(self._driver, 60).until(EC.element_to_be_clickable((By.XPATH, locator)))
            else:
               WebDriverWait(self._driver, 60).until(EC.element_to_be_clickable((By.ID, locator)))

        def write_data_to_excel(self,market_id,path,start_time,end_time,from_range,to_range):
            rb=xlrd.open_workbook(path)
            wb=copy(rb)
            w_sheet=wb.get_sheet(0)
            from_range=int(from_range)
            if(from_range%2!=0):   
                for rowNo in range(from_range,int(to_range)):
                    w_sheet.write(rowNo,0,market_id)
                    w_sheet.write(rowNo,3,start_time)
                    w_sheet.write(rowNo,4,end_time)
                wb.save(path)

        def write_tender_data_to_excel(self,market_id,path,start_time,end_time,from_range,to_range):
            rb=xlrd.open_workbook(path)
            wb=copy(rb)
            w_sheet=wb.get_sheet(0)
            from_range=int(from_range)   
            for rowNo in range(from_range,int(to_range)):
                w_sheet.write(rowNo,0,market_id)
                w_sheet.write(rowNo,3,start_time)
                w_sheet.write(rowNo,4,end_time)
            wb.save(path)

        def get_data_from_spreadsheet_into_dictionary(self,filepath,sheetName) :
            """ Returns the dictionary of values of sheet from MS Excel file"""
            workbook = xlrd.open_workbook(filepath)
            all_row_dict={}
            worksheet=workbook.sheet_by_name(sheetName)
            no_of_rows=worksheet.nrows
            headersList=worksheet.row_values(int(0))
            for rowNo in range(1,int(no_of_rows)):
                each_row_dict={}
                rowValues=worksheet.row_values(int(rowNo))
                for rowIndex in range(0,len(rowValues)):
                    cell_data=rowValues[rowIndex]
                    each_row_dict[headersList[rowIndex]]= cell_data
                all_row_dict[rowValues[0]]=each_row_dict
            return all_row_dict[rowValues[0]]
                             
        def write_data_sbp(self,path,count,sbp,decrement=None):
            rb=xlrd.open_workbook(path)
            wb=copy(rb)
            w_sheet=wb.get_sheet(0)
            var=0
            value = int(var)
            for key in range(2,int(count)):
                key=int(key)
                if(key%2==0):    
                      w_sheet.write(1,key,sbp[value])
                      value=value+1
                elif(key%2!=0):  
                    if decrement==None:
                                wb.save(path)
                    else:
                           w_sheet.write(1,key,decrement[value])
                           value=value+1
                           wb.save(path)          

        def get_ms_excel_no_of_rows_count(self,filepath,sheetName=None):
            """Returns the number of rows count in the MS Excel file """
            workbook = xlrd.open_workbook(filepath)
            worksheet=workbook.sheet_by_name(sheetName)
            noofrows=worksheet.nrows
            return noofrows

        def write_data_sbp_paired_bidding(self,path,count,sbp,decrement=None):
            rb=xlrd.open_workbook(path)
            wb=copy(rb)
            w_sheet=wb.get_sheet(0)
            var=0
            value = int(var)
            for key in range(2,int(count)):
                key=int(key)
                if(key%2==0):    
                      w_sheet.write(1,key,sbp[value])
                      w_sheet.write(2,key,sbp[value])
                      value=value+1
                elif(key%2!=0):  
                    if decrement==None:
                                wb.save(path)
                    else:
                           w_sheet.write(1,key,decrement[value])
                           value=value+1
                           wb.save(path)

        def write_data_to_upload_product(self,path,start_time,end_time,from_range,to_range):
            rb=xlrd.open_workbook(path)
            wb=copy(rb)
            w_sheet=wb.get_sheet(0)
            from_range=int(from_range)
            if(from_range%2!=0):  
                for rowNo in range(from_range,int(to_range)):
                            w_sheet.write(rowNo,3,start_time)
                            w_sheet.write(rowNo,4,end_time)
                wb.save(path)

        def wait_until_time(self,arg):
                time.sleep(int(arg))

                
