*** Variables ***
${BROWSER_NAME}    Chrome
${URL}            https://www.amazon.in
${TESTDATA_FOLDER}    ${EXECDIR}/Testdata
${LONG_WAIT}      60
${MEDIUM_WAIT}    15
${SHORT_WAIT}     5
${MAX_LOAD_WAIT}    5 min
${MINI_SHORT_WAIT}    2