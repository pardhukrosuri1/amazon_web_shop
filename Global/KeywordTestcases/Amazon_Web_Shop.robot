*** Settings ***
Suite Teardown    Take Screenshot and Close Browsers
Test Teardown     Close Browser
Resource          ../Global/super.robot

*** Test Cases ***
TC_01 Verify whether user is able to search for a product
    [Documentation]    Searching for a product in the Amazon shopping site:
    ...
    ...    1.Opening the browser and navigating to "Amazon.in" website
    ...
    ...    2.Seraching for a required \ product by entering the product name in the \ search bar and submit
    ...
    ...    3.Verifying whether product is available
    [Setup]    Test Prerequisites    TC_01    Amazon
    Comment    Navigate to Amazon shopping site
    Navigate To Amazon Website
    Comment    Search for a product
    Search Product    ${test_prerequisite_data}
    Comment    Verifying the searched results
    Validate Search Results    ${test_prerequisite_data}

TC_02 Verify whether user is able to add a product to the cart
    [Documentation]    1.Adding the product to the cart
    ...
    ...    2. Validating whether product has been added in the cart
    [Setup]    Test Prerequisites    TC_02    Amazon
    Comment    Navigate to Amazon shopping site
    Navigate To Amazon Website
    Comment    Search for a product
    Search Product    ${test_prerequisite_data}
    Comment    Verifying the searched results
    Validate Search Results    ${test_prerequisite_data}
    Comment    Navigate to add product to the cart
    Add Product To The Cart
    Comment    Validate cart items
    Validate Product Added To The Cart    ${test_prerequisite_data}

TC_03 Verify whether change in the price after increasing the product quantity
    [Documentation]    1.Increase the product quantity
    ...
    ...    2.Verify whether prices are changed after increasing the quantity
    [Setup]    Test Prerequisites    TC_03    Amazon
    Comment    Navigate to Amazon shopping site
    Navigate To Amazon Website
    Comment    Search for a product
    Search Product    ${test_prerequisite_data}
    Comment    Verifying the searched results
    Validate Search Results    ${test_prerequisite_data}
    Comment    Navigate to add product to the cart
    Add Product To The Cart
    Comment    Validate cart items
    Validate Product Added To The Cart    ${test_prerequisite_data}
    Comment    Validate cart items
    Increase Product Quantity    ${test_prerequisite_data}
    Comment    Validate price change after increasing quantity
    Validate Change In Price

TC_04 Verify whether user is able to remove a product from the cart
    [Documentation]    1.Remove the product from the cart
    ...
    ...    2.Verify whether added product has been removed from the cart
    [Setup]    Test Prerequisites    TC_04    Amazon
    Comment    Navigate to Amazon shopping site
    Navigate To Amazon Website
    Comment    Search for a product
    Search Product    ${test_prerequisite_data}
    Comment    Verifying the searched results
    Validate Search Results    ${test_prerequisite_data}
    Comment    Navigate to add product to the cart
    Add Product To The Cart
    Comment    Validate cart items
    Increase Product Quantity    ${test_prerequisite_data}
    Comment    Validate cart items
    Validate Product Added To The Cart    ${test_prerequisite_data}
    Comment    Validate price change after increasing quantity
    Validate Change In Price
    Comment    Remove product from the cart
    Remove the Product
    Comment    Check whether cart is empty
    Validate Product Is Removed    ${test_prerequisite_data}
