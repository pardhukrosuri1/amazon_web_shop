*** Settings ***
Resource          ../Global/super.robot

*** Keywords ***
Launch Browser and Navigate to URL
    [Arguments]    ${browser_name}    ${url}
    ${session}    Run Keyword And Return Status    SeleniumLibrary.Get Session Id
    Run Keyword If    ${session}==True    SeleniumLibrary.Go To    ${url}
    ...    ELSE    Launch Browser    ${url}    ${browser_name}
    SeleniumLibrary.Maximize Browser Window

Launch Browser
    [Arguments]    ${url}    ${browser_name}
    Run Keyword If    '${browser_name}'=='Chrome' or '${browser_name}'=='chrome' or '${browser_name}'=='gc'    Open Chrome Browser    ${url}
    Run Keyword If    '${browser_name}'== 'Firefox' or '${browser_name}'== 'ff' or '${browser_name}'== 'firefox'    SeleniumLibrary.Open Browser    ${url}    Firefox

Test Prerequisites
    [Arguments]    ${testcaseid}    ${sheet_name}=TestData
    ${test_prerequisite_data}    CustomLibrary.Get Ms Excel Row Values Into Dictionary Based On Key    ${TESTDATA_FOLDER}\\TestData.xls    ${testcaseid}    ${sheet_name}
    Set Global Variable    ${test_prerequisite_data}

Take Screenshot and Close Browsers
    Screenshot.Take Screenshot
    Close All Browsers

Accept Alert
    ${status}    Run Keyword And Return Status    SeleniumLibrary.Alert Should Not Be Present    timeout=${SHORT_WAIT}
    Run Keyword If    ${status}==False    SeleniumLibrary.Handle Alert    timeout=${SHORT_WAIT}

Fail and Take Screenshot
    [Arguments]    ${message}
    Capture Page Screenshot
    Fail    ${message}

Validate Alert Message
    [Arguments]    ${expected_message}
    Wait Until Time    ${SHORT_WAIT}
    ${alert_message}    SeleniumLibrary.Handle Alert    timeout=${SHORT_WAIT}
    ${status}    Run Keyword And Return Status    Should Be Equal    ${alert_message}    ${expected_message}    ${alert_message} is not equal to ${expected_message}
    Run keyword if    '${status}'=='False'    Fail and Take Screenshot    ${alert_message} is not equal to ${expected_message}
