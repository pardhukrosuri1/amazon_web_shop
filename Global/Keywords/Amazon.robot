*** Settings ***
Resource          ../Global/super.robot

*** Keywords ***
Search Product
    [Arguments]    ${test_prerequisite_data}
    SeleniumLibrary.Wait Until Element Is Visible    ${textbox.amazon.search_bar}    ${MEDIUM_WAIT}    Seach bar is not visible after waiting ${MEDIUM_WAIT} seconds
    SeleniumLibrary.Input Text    ${textbox.amazon.search_bar}    ${test_prerequisite_data}[Search_product_[0]]
    SeleniumLibrary.Click Element    ${button.amazon.search}

Validate Search Results
    [Arguments]    ${test_prerequisite_data}
    ${status}    Run Keyword And Return Status    SeleniumLibrary.Scroll Element Into View    //span[normalize-space(text())="${test_prerequisite_data}[Search_product_[0]]"]
    Run Keyword If    ${status}==True    Single Product    ${test_prerequisite_data}
    Run Keyword If    ${status}==False    Search Next Product    ${test_prerequisite_data}

Add Product To The Cart
    SeleniumLibrary.Wait Until Element Is Visible    //span[normalize-space(text())="${Product_name}"]    ${SHORT_WAIT}    Searched text for a product is not visible after waiting ${SHORT_WAIT} seconds
    SeleniumLibrary.Click Element    //span[normalize-space(text())="${Product_name}"]
    Switch Window    ${SECOND_WINDOW}
    SeleniumLibrary.Wait Until Element Is Visible    //span[normalize-space(text())="${Product_name}"]    ${SHORT_WAIT}    Searched text for a product is not visible after waiting ${SHORT_WAIT} seconds
    SeleniumLibrary.Wait Until Element Is Visible    ${button.amazon.add_to_cart}    ${MEDIUM_WAIT}    Add to cart button is not visible after waiting ${MEDIUM_WAIT} seconds
    SeleniumLibrary.Scroll Element Into View    ${button.amazon.add_to_cart}
    SeleniumLibrary.Click Element    ${button.amazon.add_to_cart}

Validate Product Added To The Cart
    [Arguments]    ${test_prerequisite_data}
    SeleniumLibrary.Wait Until Element Is Visible    ${label.amazon.added_to_cart_success_message}    ${SHORT_WAIT}    Text is not visible after waiting ${SHORT_WAIT} seconds
    ${result}    Get Text    ${label.amazon.added_to_cart_success_message}
    ${status}    Run Keyword And Return Status    Should Contain    ${result}    ${test_prerequisite_data}[Cart]
    Run Keyword If    ${status}==False    Fail and Take Screenshot    Product is not successfully added in the cart
    ${Product_price}    Get Text    ${label.amazon.single_product_price}
    Set Global Variable    ${Product_price}

Increase Product Quantity
    [Arguments]    ${test_prerequisite_data}
    Go Back
    SeleniumLibrary.Wait Until Element Is Visible    ${dropdown.amazon.product_quantity}    ${SHORT_WAIT}    Dropdown is not visible after waiting ${SHORT_WAIT} seconds
    Select From List By Label    ${dropdown.amazon.product_quantity}    ${test_prerequisite_data}[Quantity]
    SeleniumLibrary.Click Element    ${button.amazon.add_to_cart}

Validate Change In Price
    SeleniumLibrary.Wait Until Element Is Visible    ${label.amazon.multiple_product_price}    ${MEDIUM_WAIT}    Price is not visible after waiting ${MEDIUM_WAIT} seconds
    ${Increased_product_quantity_price}    Get Text    ${label.amazon.multiple_product_price}
    ${Status}    Run Keyword And Return Status    Should Not Be Equal    ${Product_price}    ${Increased_product_quantity_price}
    Run Keyword If    ${Status}==False    Fail and Take Screenshot    Product quantity has not increased and price also not chnaged

Remove the Product
    SeleniumLibrary.Wait Until Element Is Visible    ${button.amazon.cart}    ${MEDIUM_WAIT}    Cart button is not visible after waiting ${MEDIUM_WAIT} seconds
    SeleniumLibrary.Click Element    ${button.amazon.cart}
    SeleniumLibrary.Wait Until Element Is Visible    ${button.amazon.delete_items}    ${SHORT_WAIT}    Cart button is not visible after waiting ${SHORT_WAIT} seconds
    SeleniumLibrary.Click Element    ${button.amazon.delete_items}

Validate Product Is Removed
    [Arguments]    ${test_prerequisite_data}
    SeleniumLibrary.Wait Until Element Is Visible    ${label.amazon.empty_cart}    ${LONG_WAIT}    label is not visible after waiting ${LONG_WAIT} seconds
    ${cart_message}    Get Text    ${label.amazon.empty_cart}
    ${Status}    Run Keyword And Return Status    Should Contain    ${cart_message}    ${test_prerequisite_data}[Success_message]
    Run Keyword If    ${Status}==False    Fail and Take Screenshot    Your cart is not empty

Search Next Product
    [Arguments]    ${test_prerequisite_data}
    SeleniumLibrary.Wait Until Element Is Visible    ${textbox.amazon.search_bar}    ${MEDIUM_WAIT}    Seach bar is not visible after waiting ${MEDIUM_WAIT} seconds
    SeleniumLibrary.Input Text    ${textbox.amazon.search_bar}    ${test_prerequisite_data}[Search_product_[1]]
    SeleniumLibrary.Click Element    ${button.amazon.search}
    SeleniumLibrary.Scroll Element Into View    ${test_prerequisite_data}[Search_product_[1]]
    ${Product_name}    Get Text    //span[normalize-space(text())="${test_prerequisite_data}[Search_product_[1]]
    Set Global Variable    ${Product_name}

Single Product
    [Arguments]    ${test_prerequisite_data}
    ${Product_name}    Get Text    //span[normalize-space(text())="${test_prerequisite_data}[Search_product_[0]]"]
    Set Global Variable    ${Product_name}

Navigate To Amazon Website
    Launch Browser and Navigate to URL    ${BROWSER_NAME}    ${URL}
