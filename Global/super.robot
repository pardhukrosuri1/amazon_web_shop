*** Settings ***
Library           SeleniumLibrary
Library           Collections
Library           Screenshot
Library           DateTime
Library           OperatingSystem
Resource          global_variables.robot
Resource          application_variables.robot
Resource          ../Keywords/common.robot
Resource          ../Keywords/Amazon.robot
Library           ../Library/CustomLibrary.py
Resource          ../ObjectRepository/buttons.robot
Resource          ../ObjectRepository/dropdowns.robot.robot
Resource          ../ObjectRepository/label.robot.robot
Resource          ../ObjectRepository/textboxes.robot.robot
